package com.degoos.chess.user;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

/**
 * Represents an app's user.
 */
public class User {

    private String name;
    private String password;

    private int solvedProblems, triedProblems;

	/**
	 * Creates an user using a name and a password.
	 * @param name the name.
	 * @param password the password.
	 */
	public User(String name, String password) {
        this.name = name;
        this.password = password;
        this.solvedProblems = 0;
        this.triedProblems = 0;
    }

    /**
     * Creates an user using a name, a password and the amount of solved and tried problems.
     * @param name the name.
     * @param password the password.
     * @param solvedProblems the amount of solved problems.
     * @param triedProblems the amount os tried problems.
     */
    public User(String name, String password, int solvedProblems, int triedProblems) {
        this.name = name;
        this.password = password;
        this.solvedProblems = solvedProblems;
        this.triedProblems = triedProblems;
    }

    /**
     * Returns the name of the user.
     *
     * @return the name of the user.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the password of the user.
     *
     * @return the password of the user.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password of the user if the given string is not null.
     *
     * @param password the password.
	 * @return whether the password was changed.
     */
    public boolean setPassword(String password) {
        if (password == null) return false;
        this.password = password;
        return true;
    }

	/**
	 * Returns the amount of problems the user has solved.
	 * @return the amount of problems the user has solved.
	 */
	public int getSolvedProblems() {
        return solvedProblems;
    }

	/**
	 * Sets the amount of problems the user has solved.
	 * @param solvedProblems the amount of problems the user has solved.
	 */
	public void setSolvedProblems(int solvedProblems) {
        this.solvedProblems = solvedProblems;
    }

	/**
	 * Adds a solved problem.
	 */
	public void addSolvedProblem() {
        solvedProblems++;
    }

	/**
	 * Returns the amount of problems the user has tried.
	 * @return the amount of problems the user has tried.
	 */
	public int getTriedProblems() {
        return triedProblems;
    }

	/**
	 * Sets the amount of problems the user has tried.
	 * @param triedProblems the amount of problems the user has tried.
	 */
	public void setTriedProblems(int triedProblems) {
        this.triedProblems = triedProblems;
    }

	/**
	 * Adds a tried problem.
	 */
	public void addTriedProblem() {
        triedProblems++;
    }

	/**
	 * Returns the win rate of the player in percentage (0 - 100).
	 * @return the win rate.
	 */
	public float getWinRate() {
        return triedProblems == 0 ? Float.NaN : solvedProblems * 100f / triedProblems;
    }

	/**
	 * Saves the user in the given folder. This creates a file user if not present.
	 * @param folder the folder.
	 */
	public void save(File folder) {
        //Saves the user on a binary file.
        File file = new File(folder, name + ".user");
        try {
            DataOutputStream out = new DataOutputStream(new FileOutputStream(file));

            out.writeUTF(name);
            out.writeUTF(password);
            out.writeInt(triedProblems);
            out.writeInt(solvedProblems);

            out.close();
        } catch (IOException e) {
            System.err.println("Error while saving user " + name);
            e.printStackTrace();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
