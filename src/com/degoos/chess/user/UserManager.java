package com.degoos.chess.user;

import com.degoos.chess.Chess;
import com.degoos.chess.util.FileUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Represents an user manager.
 */
public class UserManager {

	private File folder;
	private Map<String, User> users;

	public UserManager() {
		folder = new File("users");
		FileUtils.checkFolder(folder);
		users = new HashMap<>();
	}

	public Map<String, User> getUsers() {
		return users;
	}

	public boolean addUser(User user) {
		if (users.containsKey(user.getName())) return false;
		users.put(user.getName(), user);
		return true;
	}

	public Optional<User> getUser(String name) {
		return Optional.ofNullable(users.getOrDefault(name, null));
	}

	public void loadSavedUsers() {
		File[] files = folder.listFiles();
		if (files == null) return;
		for (File file : files) {
			Optional<User> optional = Chess.getUserLoader().load(file);
			if (optional.isPresent())
				users.put(optional.get().getName(), optional.get());
			else System.err.println("Error while loading user from file " + file.getName());
		}
	}


	public void saveUsers() {
		users.values().forEach(target -> target.save(folder));
	}

}
