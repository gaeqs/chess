package com.degoos.chess.exercise;

import com.degoos.chess.game.Table;
import com.degoos.chess.user.User;
import com.degoos.chess.util.ScannerUtils;

import java.io.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;

/**
 * Represents an exercise.
 */
public class Exercise {

    private String fileName;
    private String name;
    private Table table;
    private String solution;

    private Set<User> winners;
    private int triedTimes, solvedTimes;

    /**
     * Creates an exercise using all its data.
     *
     * @param fileName    the name of the exercise's name.
     * @param name        the name.
     * @param table       the table of the exercise.
     * @param solution    the solution.
     * @param triedTimes  the amount of times the exercise was tried.
     * @param solvedTimes the amount of times the exercise was solved.
     * @param winners     the collection of player that have solved the exercise.
     */
    public Exercise(String fileName, String name, Table table, String solution, int triedTimes, int solvedTimes, Set<User> winners) {
        this.fileName = fileName;
        this.name = name;
        this.table = table;
        this.solution = solution;
        this.triedTimes = triedTimes;
        this.solvedTimes = solvedTimes;
        this.winners = winners == null ? new HashSet<>() : winners;

        if (!fileName.toLowerCase().endsWith(".txt"))
            this.fileName += ".txt";
    }


    /**
     * Returns the name of the exercise.
     *
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the table of the exercise.
     *
     * @return the table.
     */
    public Table getTable() {
        return table;
    }

    /**
     * Returns the solution of the exercise.
     *
     * @return the solution.
     */
    public String getSolution() {
        return solution;
    }

    /**
     * Returns the collection of players that have solved the exercise.
     *
     * @return the collection.
     */
    public Set<User> getWinners() {
        return winners;
    }

    /**
     * Returns the amount of times the exercise was tried.
     *
     * @return the amount of times the exercise was tried.
     */
    public int getTriedTimes() {
        return triedTimes;
    }

    /**
     * Returns the amount of times the exercise was solved.
     *
     * @return the amount of times the exercise was solved.
     */
    public int getSolvedTimes() {
        return solvedTimes;
    }

    /**
     * Returns the solve rate of the exercise in percentage (0 - 100).
     *
     * @return the solve rate,
     */
    public float getSolveRate() {
        return triedTimes == 0 ? Float.NaN : solvedTimes * 100f / triedTimes;
    }

    /**
     * Runs the exercise. Used by the text GUI.
     *
     * @param scanner the scannner of the console.
     * @param user    the user that is trying the exercise.
     */
    public void run(Scanner scanner, User user) {
        Table exerciseTable = new Table(table);
        exerciseTable.print(false);
        System.out.println();
        System.out.print("Enter the solution: ");
        String solution = ScannerUtils.nextLine(scanner);
        System.out.println(solve(solution, user) ? "You win!" : "You lose!");
    }

    /**
     * Tries to solve the exercise. This method
     * also updates all stats.
     *
     * @param solution the solution given by the user.
     * @param user     the user.
     * @return whether the solution is correct.
     */
    public boolean solve(String solution, User user) {
        triedTimes++;
        user.addTriedProblem();
        if (this.solution.equals(solution)) {
            user.addSolvedProblem();
            solvedTimes++;
            winners.add(user);
            return true;
        }
        return false;
    }

    /**
     * Saves the exercise into the given folder.
     * This creates two files: a txt file with the table, name and solution and a .stats binary file with the statistics.
     *
     * @param folder the folder.
     */
    public void save(File folder) {
        File file = new File(folder, fileName);
        File stats = new File(folder, fileName.substring(0, fileName.lastIndexOf('.') + 1) + "stats");
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));

            writer.write(name + '\n');
            writer.write(solution + '\n');
            table.save(writer);
            writer.close();

            if (stats.exists())
                stats.delete();
            stats.createNewFile();

            DataOutputStream stream = new DataOutputStream(new FileOutputStream(stats));

            stream.writeInt(triedTimes);
            stream.writeInt(solvedTimes);
            for (User target : winners) {
                stream.writeUTF(target.getName());
            }
            stream.close();

        } catch (IOException e) {
            System.err.println("Error while saving exercise " + name);
            e.printStackTrace();
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exercise exercise = (Exercise) o;
        return Objects.equals(name, exercise.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
