package com.degoos.chess.exercise;

import com.degoos.chess.Chess;
import com.degoos.chess.loader.ExerciseLoadResult;
import com.degoos.chess.loader.ExerciseLoadResultType;
import com.degoos.chess.util.FileUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

/**
 * Represents a exercise manager. This class stores a list of exercises.
 */
public class ExerciseManager {

    private File folder;
    private Map<String, Exercise> exercises;

    /**
     * Creates an exercise manager.
     */
    public ExerciseManager() {
        folder = new File("exercises");
        FileUtils.checkFolder(folder);
        exercises = new HashMap<>();
    }

    /**
     * Adds an exercise to the manager.
     * If a exercise with the same name is already found,
     * the given exercise won't be added.
     *
     * @param exercise the exercise.
     * @return whether the exercise was added.
     */
    public boolean addExercise(Exercise exercise) {
        if (exercise == null) return false;
        if (exercises.containsKey(exercise.getName())) return false;
        exercises.put(exercise.getName(), exercise);
        return true;
    }

    /**
     * Returns all exercises.
     *
     * @return all exercises.
     */
    public Map<String, Exercise> getExercises() {
        return exercises;
    }

    /**
     * Returns an exercise by its name if present.
     *
     * @param name the name of the exercise.
     * @return the exercise of present.
     */
    public Optional<Exercise> getExercise(String name) {
        return Optional.ofNullable(exercises.getOrDefault(name, null));
    }

    /**
     * Returns a random exercise.
     *
     * @return the exercise if there's any.
     */
    public Optional<Exercise> getRandomExercise() {
        if (exercises.isEmpty()) return Optional.empty();

        int i = new Random().nextInt(exercises.size());
        int current = 0;

        for (Exercise value : exercises.values()) {
            if (i == current) return Optional.of(value);
            current++;
        }

        return Optional.empty();
    }

    /**
     * Loads all saved exercises.
     */
    public void loadSavedExercises() {
        File[] files = folder.listFiles();
        if (files == null) return;
        for (File file : files) {
            if (!file.getName().toLowerCase().endsWith(".txt")) continue;
            ExerciseLoadResult result = Chess.getExerciseLoader().load(file,
                    new File(folder, file.getName().substring(0, file.getName().length() - 3) + "stats"));
            if (result.getType() == ExerciseLoadResultType.SUCCESS)
                addExercise(result.getExercise());
            else {
                System.err.println("Error while loading exercise from file " + file.getName() + ":");
                System.err.println(result.getType() + ": " + result.getDetails());
            }
        }
    }

    /**
     * Saves all exercises.
     */
    public void saveExercises() {
        exercises.values().forEach(target -> target.save(folder));
    }
}
