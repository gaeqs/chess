package com.degoos.chess;

import com.degoos.chess.exercise.ExerciseManager;
import com.degoos.chess.gui.login.LoginMenu;
import com.degoos.chess.gui.main.MainMenu;
import com.degoos.chess.loader.ExerciseLoader;
import com.degoos.chess.loader.ExerciseStatsLoader;
import com.degoos.chess.loader.UserLoader;
import com.degoos.chess.swing.JLogin;
import com.degoos.chess.swing.JMain;
import com.degoos.chess.user.User;
import com.degoos.chess.user.UserManager;

import java.awt.*;
import java.util.Random;
import java.util.Scanner;

public class Chess {

    private static ExerciseLoader exerciseLoader;
    private static ExerciseStatsLoader exerciseStatsLoader;
    private static UserLoader userLoader;

    private static UserManager userManager;
    private static ExerciseManager exerciseManager;
    private static User currentUser;

    public static void main(String[] args) {
        initInstances();

        //Get debug and console arguments.
        boolean[] data = getStartData(args);

        Scanner scanner = new Scanner(System.in);
        if (data[1]) {
            createDebugUsers();
            if (data[0]) {
                new MainMenu(scanner).run();
                Chess.getExerciseManager().saveExercises();
                Chess.getUserManager().saveUsers();
            } else {
                JMain.create(new Point(200, 200));
            }
        } else {
            if (data[0]) {
                new LoginMenu(scanner).run();
                Chess.getExerciseManager().saveExercises();
                Chess.getUserManager().saveUsers();
            } else {
                JLogin.create();
            }
        }
    }

    private static void initInstances() {
        exerciseLoader = new ExerciseLoader();
        exerciseStatsLoader = new ExerciseStatsLoader();
        userLoader = new UserLoader();

        userManager = new UserManager();
        exerciseManager = new ExerciseManager();

        userManager.loadSavedUsers();
        exerciseManager.loadSavedExercises();
    }

    private static boolean[] getStartData(String[] arguments) {
        //0 - CONSOLE GUI, 1 - DEBUG
        boolean[] data = new boolean[2];
        for (String argument : arguments) {
            if (argument.equalsIgnoreCase("--console"))
                data[0] = true;
            if (argument.equalsIgnoreCase("--debug"))
                data[1] = true;
        }
        return data;
    }

    private static void createDebugUsers() {
        currentUser = new User("Debug", "debug");
        userManager.addUser(currentUser);

        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            User user = new User("Dummy " + i, "dummy");

            int count = random.nextInt(100);
            for (int j = 0; j < count; j++) {
                exerciseManager.getRandomExercise().ifPresent(target -> target.solve(random.nextBoolean() ? null : target.getSolution(), user));
            }

            userManager.addUser(user);
        }
    }

    public static ExerciseLoader getExerciseLoader() {
        return exerciseLoader;
    }

    public static UserLoader getUserLoader() {
        return userLoader;
    }

    public static UserManager getUserManager() {
        return userManager;
    }

    public static ExerciseManager getExerciseManager() {
        return exerciseManager;
    }

    public static ExerciseStatsLoader getExerciseStatsLoader() {
        return exerciseStatsLoader;
    }

    public static User getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(User currentUser) {
        Chess.currentUser = currentUser;
    }
}
