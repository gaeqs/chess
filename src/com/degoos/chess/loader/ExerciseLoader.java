package com.degoos.chess.loader;

import com.degoos.chess.Chess;
import com.degoos.chess.exercise.Exercise;
import com.degoos.chess.game.Position;
import com.degoos.chess.game.Table;
import com.degoos.chess.game.Team;
import com.degoos.chess.game.piece.*;
import com.degoos.chess.user.User;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Represents a exercise loader.
 */
public class ExerciseLoader {

    /**
     * Loads an exercise.
     *
     * @param path      the exercise file path.
     * @param statsPath the exercise's stats file path or null if not present.
     * @return the result of the load, with the exercise inside.
     */
    public ExerciseLoadResult load(String path, String statsPath) {
        try {
            return load(new File(path).getName(), new FileReader(path), statsPath == null ? null : new File(statsPath));
        } catch (Exception e) {
            e.printStackTrace();
            return new ExerciseLoadResult(null, ExerciseLoadResultType.BAD_FILE, "Error while loading a file.");
        }
    }

    /**
     * Loads an exercise.
     *
     * @param file  the exercise file.
     * @param stats the exercise's stats file or null if not present.
     * @return the result of the load, with the exercise inside.
     */
    public ExerciseLoadResult load(File file, File stats) {
        try {
            return load(file.getName(), new FileReader(file), stats);
        } catch (Exception e) {
            e.printStackTrace();
            return new ExerciseLoadResult(null, ExerciseLoadResultType.BAD_FILE, "Error while loading a file.");
        }
    }

    /**
     * Loads an exercise.
     *
     * @param fileName   the name of the exercise's file.
     * @param fileReader the exercise data.
     * @param stats      the exercise's stats file or null if not present.
     * @return the result of the load, with the exercise inside.
     */
    private ExerciseLoadResult load(String fileName, FileReader fileReader, File stats) {
        try {
            BufferedReader reader = new BufferedReader(fileReader);

            //Creates the table and gets the name and the solution.
            Table table = new Table();
            String name = reader.readLine();
            String solution = reader.readLine();


            //Loop loading all pieces of the table.
            String line;
            King blackKing = null;
            int count = 0;
            while ((line = readFixedLine(reader)) != null) {
                count++;
                if (line.isEmpty() || line.startsWith("//")) continue;

                if (line.equalsIgnoreCase("end")) break;
                if (line.length() != 3 && line.length() != 4) return sendError(count, line, "Bad line format.");

                Team team = Team.parse(line.charAt(0));
                if (team == null) return sendError(count, line, "Team not found.");

                Position position = parsePosition(line.charAt(line.length() == 4 ? 2 : 1), line.charAt(line.length() == 4 ? 3 : 2));
                if (position == null) return sendError(count, line, "Invalid position.");

                Piece piece = Piece.createPiece(table, team, line.length() == 4 ? line.charAt(1) : 'P', position);
                if (piece == null) return sendError(count, line, "Invalid piece.");

                if (piece instanceof Pawn && piece.getPosition().getY() == (piece.getTeam() == Team.WHITE ? 7 : 0))
                    return sendError(count, line, "There's a pawn in its opposite side.");

                if (piece instanceof King && piece.getTeam() == Team.BLACK) {
                    blackKing = (King) piece;
                }

                table.addPiece(piece);
            }

            //Checks if the table has a legal amount of pieces.
            String illegalAmount = isPiecesAmountLegal(table);
            if (illegalAmount != null) {
                table.print(true);
                return new ExerciseLoadResult(null, ExerciseLoadResultType.BAD_EXERCISE_FORMAT, illegalAmount);
            }

            //Checks if the solution has a ++ at the end.
            if (!solution.endsWith("++")) {
                return new ExerciseLoadResult(null, ExerciseLoadResultType.INCORRECT_SOLUTION, "Missing ++.");
            }

            //Checks if the solution is valid.
            String invalidSolution = isSolutionCorrect(solution, table, blackKing);
            if (invalidSolution != null) {
                table.print(true);
                return new ExerciseLoadResult(null, ExerciseLoadResultType.INCORRECT_SOLUTION,
                        "Solution is incorrect! " + invalidSolution + " " + solution);
            }


            //Loads all stats, if present.
            int triedTimes = 0;
            int solvedTimes = 0;
            Set<User> users = null;

            Object[] statsData = Chess.getExerciseStatsLoader().load(stats);
            if (statsData != null) {
                triedTimes = (int) statsData[0];
                solvedTimes = (int) statsData[1];
                users = (Set<User>) statsData[2];
            }

            //Returns the exercise.
            Exercise exercise = new Exercise(fileName, name, table, solution, triedTimes, solvedTimes, users);
            return new ExerciseLoadResult(exercise, ExerciseLoadResultType.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            return new ExerciseLoadResult(null, ExerciseLoadResultType.BAD_FILE, e.getMessage());
        }
    }

    /**
     * Reads a line from an {@link BufferedReader} and threats it, removing all jump lines and spaces.
     *
     * @param reader the {@link BufferedReader} to read.
     * @return the line.
     * @throws IOException when the read fails.
     */
    private String readFixedLine(BufferedReader reader) throws IOException {
        String line = reader.readLine();
        if (line == null) return null;
        return line.replace("\n", "").replace("\r", "").trim();
    }

    /**
     * Parses two chars to a {@link Position}.
     *
     * @param xc the x value (a - h).
     * @param yc the y value (1 - 8).
     * @return the position, or null if any error occurs.
     */
    private Position parsePosition(char xc, char yc) {
        try {
            return new Position(xc, yc);
        } catch (IllegalArgumentException ex) {
            return null;
        }
    }

    /**
     * Check the amount of pieces on a table.
     * If the amount of pieces in the table is legal returns null. Else returns a description.
     *
     * @param table the table to check.
     * @return null or a description.
     */
    private String isPiecesAmountLegal(Table table) {
        for (Team team : Team.values()) {
            String teamName = team.toString().toLowerCase();
            List<Piece> pieces = team == Team.WHITE ? table.getWhitePieces() : table.getBlackPieces();
            if (pieces.stream().filter(target -> target instanceof King).count() != 1)
                return "Invalid " + teamName + " king amount.";

            long pawnCount = pieces.stream().filter(target -> target instanceof Pawn).count();

            if (pawnCount > 8)
                return "Invalid " + teamName + " pawn amount.";
            long possibleTransformations = 8 - pawnCount;

            possibleTransformations -= Math.max(0, pieces.stream().filter(target -> target instanceof Bishop).count() - 2);
            possibleTransformations -= Math.max(0, pieces.stream().filter(target -> target instanceof Knight).count() - 2);
            possibleTransformations -= Math.max(0, pieces.stream().filter(target -> target instanceof Rook).count() - 2);

            possibleTransformations -= Math.max(0, pieces.stream().filter(target -> target instanceof Queen).count() - 1);

            if (possibleTransformations < 0) return "Invalid " + teamName + " transformations.";
        }
        return null;
    }

    /**
     * Checks if the solution of the exercise is correct. This parses the solution,
     * moves the piece and checks if the black king is in checkmate.
     * If the solution is correct returns null. Else returns a description.
     *
     * @param solution  the solution of the exercise.
     * @param table     the table of the exercise.
     * @param blackKing the black king.
     * @return null or a description.
     */
    private String isSolutionCorrect(String solution, Table table, King blackKing) {
        if (solution.length() < 4) return "Invalid solution length";
        solution = solution.substring(0, solution.length() - 2);

        boolean capturing = false;
        boolean transformPawn = false;
        char transformPawnTo = 0;
        char pieceRepresentation = 0;
        int column = -1;

        //Loads the pawn transformation, of present.
        if (solution.charAt(solution.length() - 2) == '=') {
            transformPawn = true;
            transformPawnTo = Character.toLowerCase(solution.charAt(solution.length() - 1));

            //If king, pawn or illegal, return.
            if (transformPawnTo == 'p' || transformPawnTo == 'k' || !Piece.isLegalCharacter(transformPawnTo))
                return "Invalid transformation.";

            solution = solution.substring(0, solution.length() - 2);
        }
        if (solution.length() < 2) return "Invalid solution length";

        //Loads the destination.
        Position position = parsePosition(solution.charAt(solution.length() - 2),
                solution.charAt(solution.length() - 1));
        if (position == null) return "Invalid position. ";
        solution = solution.substring(0, solution.length() - 2);

        //Loads whether the movements is capturing an enemy piece.
        if (!solution.isEmpty() && Character.toLowerCase(solution.charAt(solution.length() - 1)) == 'x') {
            capturing = true;
            solution = solution.substring(0, solution.length() - 1);
        }

        //Loads the specification, if present.
        if (!solution.isEmpty()) {
            if (solution.length() > 1) return "Invalid solution length";
            char c = solution.charAt(0);

            //Piece
            if (Character.isUpperCase(c)) {
                pieceRepresentation = c;
            }
            //Position
            else {
                Position filterPosition = parsePosition(c, '1');
                if (filterPosition == null) return "Invalid position filter.";
                column = filterPosition.getX();
            }
        }

        //Gets the piece to move.
        Piece piece = getPiece(table, pieceRepresentation, column, position);
        if (piece == null) return "Invalid piece.";

        //Check transformation.
        if (transformPawn && !(piece instanceof Pawn)) return "Invalid transformation.";
        if (!transformPawn && piece instanceof Pawn && position.getY() == (piece.getTeam() == Team.WHITE ? 7 : 0))
            return "Invalid pawn position: no transformation found.";

        //Check if in checkmate.
        boolean hasCapture = table.move(piece, position, transformPawnTo).isPresent();
        if (hasCapture != capturing) {
            table.revertLastMovement();
            return capturing ? "Movements is not capturing a piece." : "Movement is capturing a piece.";
        }

        boolean correct = blackKing.inCheckmate();
        table.revertLastMovement();

        return correct ? null : "King is not in checkmate.";
    }

    /**
     * Returns the piece to move.
     *
     * @param table          the table.
     * @param representation the piece representation.
     * @param column         the column specification or -1.
     * @param position       the position to move.
     * @return the piece or null.
     */
    private Piece getPiece(Table table, char representation, int column, Position position) {
        Predicate<Piece> predicate = target -> target.getPossibleMovements().contains(position);

        if (representation != 0) {
            predicate = updatePredicateWithPiece(representation, predicate);
            if (predicate == null) return null;
        }

        if (column != -1) predicate = predicate.and(target -> target.getPosition().getX() == column);

        List<Piece> list = table.getWhitePiecesThat(predicate);
        if (list.size() != 1) {

            //If there are more than one piece, check if there's just one pawn. Pawns have priority.
            list = list.stream().filter(target -> target instanceof Pawn).collect(Collectors.toList());
            if (list.size() == 1) {
                return list.get(0);
            }
            return null;
        }
        return list.get(0);
    }

    /**
     * Update the given predicate adding the piece representation filter.
     *
     * @param representation the piece representation.
     * @param old            the old {@link Predicate<Piece>}.
     * @return the new {@link Predicate<Piece>}.
     */
    private Predicate<Piece> updatePredicateWithPiece(char representation, Predicate<Piece> old) {
        switch (representation) {
            case 'B':
                return old.and(target -> target instanceof Bishop);
            case 'K':
                return old.and(target -> target instanceof King);
            case 'N':
                return old.and(target -> target instanceof Knight);
            case 'P':
                return old.and(target -> target instanceof Pawn);
            case 'Q':
                return old.and(target -> target instanceof Queen);
            case 'R':
                return old.and(target -> target instanceof Rook);
            default:
                return null;
        }
    }

    /**
     * Returns a failed {@link ExerciseLoadResult} with a default error description.
     *
     * @param lineNumber the line number.
     * @param line       the line.
     * @param details    the details.
     * @return the failed {@link ExerciseLoadResult}.
     */
    private ExerciseLoadResult sendError(int lineNumber, String line, String details) {
        return new ExerciseLoadResult(null, ExerciseLoadResultType.BAD_EXERCISE_FORMAT, "Error in line " + lineNumber + " (" + line + ") " + details);
    }

}
