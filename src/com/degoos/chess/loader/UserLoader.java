package com.degoos.chess.loader;

import com.degoos.chess.user.User;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Optional;

/**
 * Represents an user loader.
 */
public class UserLoader {

    /**
     * Loads an user using a file path.
     *
     * @param path the file path.
     * @return the user if present.
     */
    public Optional<User> load(String path) {
        try {
            return load(new File(path).getName());
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    /**
     * Loads an user using a file.
     *
     * @param file the file.
     * @return the user if present.
     */
    public Optional<User> load(File file) {
        try {
            DataInputStream in = new DataInputStream(new FileInputStream(file));

            String name = in.readUTF();
            String password = in.readUTF();
            int triedProblems = in.readInt();
            int solvedProblems = in.readInt();
            in.close();

            return Optional.of(new User(name, password, solvedProblems, triedProblems));

        } catch (Exception ex) {
            ex.printStackTrace();
            return Optional.empty();
        }
    }

}
