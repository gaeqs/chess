package com.degoos.chess.loader;

/**
 * Represents the load result type of an {@link ExerciseLoadResult}.
 */
public enum ExerciseLoadResultType {

    SUCCESS, BAD_FILE, BAD_EXERCISE_FORMAT, INCORRECT_SOLUTION

}
