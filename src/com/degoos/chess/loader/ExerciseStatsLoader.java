package com.degoos.chess.loader;

import com.degoos.chess.Chess;
import com.degoos.chess.user.User;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Set;

public class ExerciseStatsLoader {

    public Object[] load(File file) {
        if (file == null || !file.isFile()) return null;

        try {
            return load(new DataInputStream(new FileInputStream(file)));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object[] load(DataInputStream stream) {
        try {
            Object[] objects = new Object[3];
            objects[0] = stream.readInt();
            objects[1] = stream.readInt();

            Set<User> users = new HashSet<>();
            while (stream.available() > 0) {
                Chess.getUserManager().getUser(stream.readUTF()).ifPresent(users::add);
            }
            objects[2] = users;
            return objects;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
