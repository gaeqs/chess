package com.degoos.chess.loader;

import com.degoos.chess.exercise.Exercise;

/**
 * Represents an exercise load result.
 */
public class ExerciseLoadResult {

	private Exercise exercise;
	private ExerciseLoadResultType type;
	private String details;

	/**
	 * Creates an exercise load result using an exercise (or null), a result and details.
	 * @param exercise the exercise.
	 * @param type the result type.
	 * @param details the details.
	 */
	public ExerciseLoadResult(Exercise exercise, ExerciseLoadResultType type, String details) {
		this.exercise = exercise;
		this.type = type;
		this.details = details;
	}

	/**
	 * Returns the exercise or null.
	 * @return the exercise or null.
	 */
	public Exercise getExercise() {
		return exercise;
	}

	/**
	 * Returns the result type.
	 * @return the result type.
	 */
	public ExerciseLoadResultType getType() {
		return type;
	}

	/**
	 * Returns the details.
	 * @return the details.
	 */
	public String getDetails() {
		return details;
	}
}
