package com.degoos.chess.swing;

import com.degoos.chess.Chess;
import com.degoos.chess.user.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Optional;

public class JRegister extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nameField;
    private JPasswordField password;
    private JPasswordField repeatPassword;
    private JLabel error;

    private JRegister() {
        setTitle("Sign up");
        setContentPane(contentPane);
        setModal(true);
        setResizable(false);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(e -> onCancel(),
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        String name = nameField.getText();
        Optional<User> user = Chess.getUserManager().getUser(name);
        if(user.isPresent()) {
            error.setText("User already exists!");
            return;
        }

        String first = new String(password.getPassword());
        String second = new String(repeatPassword.getPassword());

        if(first.isEmpty()) {
            error.setText("Password cannot be empty!");
            return;
        }

        if(!first.equals(second)) {
            error.setText("Passwords don't match!");
            return;
        }

        User newUser = new User(name, first);
        Chess.getUserManager().addUser(newUser);

        dispose();
    }

    private void onCancel() {
        dispose();
    }

    static void create(Point location) {
        JRegister dialog = new JRegister();
        dialog.pack();
        dialog.setLocation(location);
        dialog.setVisible(true);
    }
}
