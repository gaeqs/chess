package com.degoos.chess.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class JMessage extends JDialog {
	private JPanel contentPane;
	private JButton buttonOK;
	private JLabel text;

	public JMessage(String text) {
		setContentPane(contentPane);
		setModal(true);
		setResizable(false);
		getRootPane().setDefaultButton(buttonOK);

		this.text.setText(text);
		buttonOK.addActionListener(e -> onOK());
	}

	private void onOK() {
		dispose();
	}

	public static void create(Point location, String text) {
		JMessage dialog = new JMessage(text);
		dialog.pack();
		dialog.setLocation(location);
		dialog.setVisible(true);
	}
}
