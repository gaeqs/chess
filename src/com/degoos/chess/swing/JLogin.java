package com.degoos.chess.swing;

import com.degoos.chess.Chess;
import com.degoos.chess.user.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Optional;

public class JLogin extends JFrame {
	private JTextField name;
	private JPasswordField password;
	private JPanel panel;
	private JButton login;
	private JButton register;
	private JLabel error;

	private boolean exitOnClose;

	public JLogin() {
		exitOnClose = true;
		setTitle("Login");
		setContentPane(panel);
		setResizable(false);

		login.addActionListener(e -> onLogin());

		register.addActionListener(e -> JRegister.create(getLocation()));

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);


		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				finishProgram();
			}
		});

		panel.registerKeyboardAction(e -> finishProgram(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

	}

	public JPanel getPanel() {
		return panel;
	}

	private void onLogin() {
		String nameText = name.getText();
		String passwordText = new String(password.getPassword());

		Optional<User> optional = Chess.getUserManager().getUser(nameText);
		if (!optional.isPresent()) {
			error.setText("User not found!");
			return;
		}

		User user = optional.get();
		if (!user.getPassword().equals(passwordText)) {
			error.setText("Incorrect password!");
			return;
		}

		exitOnClose = false;
		Chess.setCurrentUser(user);
		dispose();
		JMain.create(getLocation());
	}

	private void finishProgram() {
		if (exitOnClose) System.exit(0);
	}

	public static void create() {
		JLogin login = new JLogin();
		login.setLocation(new Point(200, 200));
		login.pack();
		login.setVisible(true);
	}
}
