package com.degoos.chess.swing;

import com.degoos.chess.Chess;
import com.degoos.chess.swing.main.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

public class JMain extends JFrame {
	private JPanel panel;
	private JButton solveRandomExerciseButton;
	private JButton showStatisticsButton;
	private JButton showExercisesStatisticsButton;
	private JButton showRankingButton;
	private JButton uploadAnExerciseButton;

	public JMain() {
		setTitle("Chess");
		setContentPane(panel);
		setResizable(false);
		solveRandomExerciseButton.addActionListener(e -> JSolveExercise.create(getLocation()));
		showStatisticsButton.addActionListener(e -> JStatistics.create(getLocation(), Chess.getCurrentUser()));
		showExercisesStatisticsButton.addActionListener(e -> JExerciseStatistics.create(getLocation()));
		showRankingButton.addActionListener(e -> JRanking.create(getLocation(), new ArrayList<>(Chess.getUserManager().getUsers().values())));
		uploadAnExerciseButton.addActionListener(e -> JUploadExercise.create(getLocation()));

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				onClose();
			}
		});

		panel.registerKeyboardAction(e -> onClose(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
	}


	private void onClose () {
		Chess.getExerciseManager().saveExercises();
		Chess.getUserManager().saveUsers();
		System.exit(0);
	}

	public static void create(Point location) {
		JMain main = new JMain();
		main.pack();
		main.setLocation(location);
		main.setVisible(true);
	}
}
