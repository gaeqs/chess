package com.degoos.chess.swing.main;

import com.degoos.chess.Chess;
import com.degoos.chess.exercise.Exercise;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class JExerciseStatistics extends JDialog {
	private JPanel panel;
	private JPanel listPanel;
	private JList<String> exerciseList;
	private JPanel infoPanel;
	private JLabel triedField;
	private JLabel solvedField;
	private JLabel rateField;
	private JLabel nameField;
	private JPanel buttonPanel;
	private JButton solveButton;
	private JButton winnersButton;
	private JTextArea preview;

	private List<Exercise> exercises;
	private int selectedIndex;
	private Exercise selectedExercise;

	public JExerciseStatistics() {
		setTitle("Exercises");
		setContentPane(panel);
		setModal(true);
		setResizable(false);

		selectedIndex = -1;
		selectedExercise = null;

		exercises = new ArrayList<>(Chess.getExerciseManager().getExercises().values());
		exercises.sort(Comparator.comparing(Exercise::getName));
		exerciseList.setListData(exercises.stream().map(Exercise::getName).toArray(String[]::new));
		solveButton.setEnabled(selectedExercise != null);
		winnersButton.setEnabled(selectedExercise != null);

		exerciseList.addListSelectionListener(e -> {
			if (e.getValueIsAdjusting())
				selectExercise(exerciseList.getSelectedIndex());
		});


		solveButton.addActionListener(e -> JSolveExercise.create(getLocation(), selectedExercise));
		winnersButton.addActionListener(e -> JRanking.create(getLocation(), new ArrayList<>(selectedExercise.getWinners())));

		addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				updateExerciseInfo();
			}
		});
	}

	private void selectExercise(int index) {
		if (index < 0 || index > exercises.size()) return;
		selectedIndex = index;
		selectedExercise = exercises.get(selectedIndex);
		updateExerciseInfo();
		solveButton.setEnabled(selectedExercise != null);
		winnersButton.setEnabled(selectedExercise != null);
	}

	private void updateExerciseInfo() {
		nameField.setText(selectedExercise == null ? "" : selectedExercise.getName());
		triedField.setText(selectedExercise == null ? "" : selectedExercise.getTriedTimes() + (selectedExercise.getTriedTimes() == 1 ? " time" : " times"));
		solvedField.setText(selectedExercise == null ? "" : selectedExercise.getSolvedTimes() + (selectedExercise.getSolvedTimes() == 1 ? " time" : " times"));

		if (selectedExercise == null) {
			rateField.setText("");
		} else {
			float rate = selectedExercise.getSolveRate();
			rateField.setText(Float.isNaN(rate) ? "XX%" : Math.round(rate) + "%");

			preview.setText(selectedExercise.getTable().toString());
		}


	}

	public static void create(Point location) {
		JExerciseStatistics dialog = new JExerciseStatistics();
		dialog.pack();
		dialog.setLocation(location);
		dialog.setVisible(true);
	}
}
