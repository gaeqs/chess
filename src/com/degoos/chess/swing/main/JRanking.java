package com.degoos.chess.swing.main;

import com.degoos.chess.Chess;
import com.degoos.chess.user.User;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class JRanking extends JDialog {
	private JPanel panel;
	private JPanel listPanel;
	private JPanel infoPanel;
	private JList<String> userList;
	private JLabel nameField;
	private JLabel triedProblemsField;
	private JLabel solvedProblemsField;
	private JLabel failedProblemsField;
	private JLabel winRateField;
	private JButton sortButton;

	private List<User> users;
	private boolean sortBySolved;
	private int selectedIndex;
	private User selectedUser;

	public JRanking(ArrayList<User> users) {
		setTitle("Ranking");
		setContentPane(panel);
		setModal(true);
		setResizable(false);

		selectedIndex = -1;
		selectedUser = null;

		this.users = users;
		sortBySolved = false;
		sort();

		userList.addListSelectionListener(e -> {
			if (e.getValueIsAdjusting())
				selectUser(userList.getSelectedIndex());
		});

		sortButton.addActionListener(e -> changeSortOrder());
	}

	private void selectUser(int index) {
		if (index < 0 || index > users.size()) return;
		selectedIndex = index;
		selectedUser = users.get(selectedIndex);
		updateUserInfo();
	}

	private void updateUserInfo() {
		nameField.setText(selectedUser == null ? "" : selectedUser.getName());
		triedProblemsField.setText(selectedUser == null ? "" : String.valueOf(selectedUser.getTriedProblems()));
		solvedProblemsField.setText(selectedUser == null ? "" : String.valueOf(selectedUser.getSolvedProblems()));
		failedProblemsField.setText(selectedUser == null ? "" : String.valueOf(selectedUser.getTriedProblems() - selectedUser.getSolvedProblems()));

		if(selectedUser == null) {
			winRateField.setText("");
		}
		else {
			float rate = selectedUser.getWinRate();
			winRateField.setText(Float.isNaN(rate) ? "XX%" : Math.round(rate) + "%");
		}


	}

	private void sort() {
		if (sortBySolved) {
			users.sort((o1, o2) -> o2.getSolvedProblems() - o1.getSolvedProblems());
		} else {
			users.sort((o1, o2) -> {
				float w1 = o1.getWinRate();
				float w2 = o2.getWinRate();
				if(Float.isNaN(w1)) return 1;
				if(Float.isNaN(w2)) return -1;
				if (w1 == w2) return 0;
				return w2 - w1 > 0 ? 1 : -1;
			});
		}
		userList.setListData(users.stream().map(User::getName).toArray(String[]::new));
	}

	private void changeSortOrder() {
		sortBySolved = !sortBySolved;
		sort();
		sortButton.setText(sortBySolved ? "Sort by win rate" : "Sort by solved problems");
		if (selectedUser != null) {
			selectedIndex = users.indexOf(selectedUser);
			userList.setSelectedIndex(selectedIndex);
		}
	}

	public static void create(Point location, ArrayList<User> users) {
		JRanking dialog = new JRanking(users);
		dialog.pack();
		dialog.setLocation(location);
		dialog.setVisible(true);
	}
}
