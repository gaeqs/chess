package com.degoos.chess.swing.main;

import com.degoos.chess.Chess;
import com.degoos.chess.loader.ExerciseLoadResult;
import com.degoos.chess.loader.ExerciseLoadResultType;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class JUploadExercise extends JDialog {


	private JPanel panel;
	private JTextArea text;

	public JUploadExercise() {
		setTitle("Exercise uploader");
		setContentPane(panel);
		setModal(true);
		setResizable(false);

		text.setText("Drop here a file with an exercise inside!");

		panel.setDropTarget(new DropTarget() {
			@Override
			public synchronized void drop(DropTargetDropEvent event) {
				handleDrop(event);
			}
		});
	}

	private void handleDrop(DropTargetDropEvent event) {
		List<File> files = new LinkedList<>();
		event.acceptDrop(DnDConstants.ACTION_REFERENCE);
		Transferable transferable = event.getTransferable();
		DataFlavor[] flavors = transferable.getTransferDataFlavors();

		for (DataFlavor flavor : flavors) {
			if (!flavor.isFlavorJavaFileListType()) continue;
			try {
				files.addAll((List<File>) transferable.getTransferData(flavor));
			} catch (UnsupportedFlavorException | IOException | ClassCastException e) {
				e.printStackTrace();
			}
		}

		StringBuilder builder = new StringBuilder();
		files.forEach(target -> handleExerciseLoad(target, builder));
		text.setText(builder.toString());
	}

	private void handleExerciseLoad(File file, StringBuilder builder) {
		ExerciseLoadResult result = Chess.getExerciseLoader().load(file, null);

		if (handleInfo(builder, result, file.getName()))
			Chess.getExerciseManager().addExercise(result.getExercise());
	}

	private boolean handleInfo(StringBuilder builder, ExerciseLoadResult result, String file) {
		if (builder.length() != 0) builder.append('\n');
		if (result.getType() == ExerciseLoadResultType.SUCCESS) {

			if (Chess.getExerciseManager().getExercise(result.getExercise().getName()).isPresent()) {
				builder.append("The exercise ").append(result.getExercise().getName()).append(" loaded from the file ")
						.append(file).append(" has a name already registered in the application!");
				return false;
			}

			builder.append("Exercise ").append(result.getExercise().getName()).append(" loaded from file ").append(file).append("!");
			return true;
		}


		builder.append("Error while loading exercise from file ").append(file).append('\n');
		builder.append(result.getType()).append(": ").append(result.getDetails());
		return false;
	}

	public static void create(Point location) {
		JUploadExercise dialog = new JUploadExercise();
		dialog.pack();
		dialog.setLocation(location);
		dialog.setVisible(true);
	}
}
