package com.degoos.chess.swing.main;

import com.degoos.chess.user.User;

import javax.swing.*;
import java.awt.*;

public class JStatistics extends JDialog {
	private JPanel contentPanel;
	private JLabel nameField;
	private JLabel triedField;
	private JLabel solvedField;
	private JLabel failedField;
	private JLabel rateField;


	public JStatistics(User user) {
		setTitle(user.getName() + "'s statistics");
		setContentPane(contentPanel);
		setModal(true);
		setResizable(false);

		nameField.setText(user.getName());
		triedField.setText(String.valueOf(user.getTriedProblems()));
		solvedField.setText(String.valueOf(user.getSolvedProblems()));
		failedField.setText(String.valueOf(user.getTriedProblems() - user.getSolvedProblems()));
		rateField.setText(Float.isNaN(user.getWinRate()) ? "XX%" : Math.round(user.getWinRate()) + "%");
	}

	public static void create(Point location, User user) {
		JStatistics dialog = new JStatistics(user);
		dialog.pack();
		dialog.setLocation(location);
		dialog.setVisible(true);
	}
}
