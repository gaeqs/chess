package com.degoos.chess.swing.main;

import com.degoos.chess.Chess;
import com.degoos.chess.exercise.Exercise;
import com.degoos.chess.swing.JMessage;

import javax.swing.*;
import java.awt.*;
import java.util.Optional;

public class JSolveExercise extends JDialog {
    private JPanel contentPane;
    private JTextArea exerciseArea;
    private JTextField solutionField;
    private JButton solveButton;

    private Exercise exercise;

    public JSolveExercise(Exercise exercise) {
        this.exercise = exercise;
        setTitle(exercise.getName());
        setContentPane(contentPane);
        setModal(true);
        setResizable(false);

        exerciseArea.setTabSize(3);
        exerciseArea.setText(exercise.getTable().toString());
        exerciseArea.setEditable(false);
        solveButton.addActionListener(e -> onSolve());
    }

    private void onSolve() {
        String solution = solutionField.getText();
        if(solution.isEmpty()) return;
        JMessage.create(getLocation(), exercise.solve(solution, Chess.getCurrentUser()) ? "You win!" : "You lose!");
        dispose();
    }

    public static void create(Point location) {
        Optional<Exercise> optional = Chess.getExerciseManager().getRandomExercise();
        if (!optional.isPresent()) return;
        create(location, optional.get());
    }

    public static void create(Point location, Exercise exercise) {
        JSolveExercise dialog = new JSolveExercise(exercise);
        dialog.pack();
        dialog.setLocation(location);
        dialog.setVisible(true);
    }
}
