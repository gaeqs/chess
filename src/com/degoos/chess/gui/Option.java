package com.degoos.chess.gui;

import java.util.Scanner;

public abstract class Option {

    private String name;

    public Option (String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract void run (Scanner scanner);
}
