package com.degoos.chess.gui;

import java.util.List;
import java.util.Scanner;

public class OptionMenu extends Menu {

    protected List<Option> options;

    public OptionMenu(Scanner scanner, List<Option> options) {
        super(scanner);
        this.options = options;
    }

    public List<Option> getOptions() {
        return options;
    }

    @Override
    public void run() {
        int option;
        do {
            printOptions();
            option = scanner.nextInt();
            if(option < 0 || option >= options.size()) continue;

            Option instance = options.get(option);
            instance.run(scanner);

        } while (option != options.size());
    }

    private void printOptions() {
        Option option;
        for (int i = 0; i < options.size(); i++) {
            option = options.get(i);
            System.out.println(i + " - " + option.getName());
        }
        System.out.println(options.size() + " - Go back.");
    }
}
