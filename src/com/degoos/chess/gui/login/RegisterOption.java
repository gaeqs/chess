package com.degoos.chess.gui.login;

import com.degoos.chess.Chess;
import com.degoos.chess.gui.Option;
import com.degoos.chess.gui.main.MainMenu;
import com.degoos.chess.user.User;
import com.degoos.chess.util.ScannerUtils;

import java.util.Optional;
import java.util.Scanner;

public class RegisterOption extends Option {

    public RegisterOption() {
        super("Register");
    }

    @Override
    public void run(Scanner scanner) {
        System.out.print("Enter the name: ");
        String name = ScannerUtils.nextLine(scanner);

        if(name.equalsIgnoreCase("exit") || name.equalsIgnoreCase("sort")) {
            System.out.println("You can't use this name!");
            return;
        }

        Optional<User> user = Chess.getUserManager().getUser(name);
        if (user.isPresent()) {
            System.out.println("User already exist!");
            return;
        }

        System.out.print("Enter the password: ");
        String password = ScannerUtils.nextLine(scanner);

        System.out.print("Enter the password again: ");
        String passwordCheck = ScannerUtils.nextLine(scanner);

        if (!password.equals(passwordCheck)) {
            System.out.println("Passwords don't match!");
            return;
        }

        User newUser = new User(name, password);
        Chess.getUserManager().addUser(newUser);
        Chess.setCurrentUser(newUser);
        new MainMenu(scanner).run();
    }
}