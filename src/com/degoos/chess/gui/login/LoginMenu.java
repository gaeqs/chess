package com.degoos.chess.gui.login;

import com.degoos.chess.gui.Option;
import com.degoos.chess.gui.OptionMenu;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class LoginMenu extends OptionMenu {

    public LoginMenu(Scanner scanner) {
        super(scanner, createOptions());
    }

    private static List<Option> createOptions() {
        List<Option> list = new LinkedList<>();
        list.add(new LoginOption());
        list.add(new RegisterOption());
        return list;
    }
}
