package com.degoos.chess.gui.login;

import com.degoos.chess.Chess;
import com.degoos.chess.gui.Option;
import com.degoos.chess.gui.main.MainMenu;
import com.degoos.chess.user.User;
import com.degoos.chess.util.ScannerUtils;

import java.util.Optional;
import java.util.Scanner;

public class LoginOption extends Option {


    public LoginOption() {
        super("Login");
    }

    @Override
    public void run(Scanner scanner) {
        System.out.print("Enter the name: ");
        String name = ScannerUtils.nextLine(scanner);
        System.out.print("Enter the password: ");
        String password = ScannerUtils.nextLine(scanner);

        Optional<User> user =  Chess.getUserManager().getUser(name);
        if(!user.isPresent()) {
            System.out.println("User not found!");
            return;
        }

        if(!user.get().getPassword().equals(password)) {
            System.out.println("Incorrect password!");
            return;
        }

        Chess.setCurrentUser(user.get());
        new MainMenu(scanner).run();
    }
}
