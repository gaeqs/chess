package com.degoos.chess.gui.ranking;

import com.degoos.chess.Chess;
import com.degoos.chess.gui.Menu;
import com.degoos.chess.gui.main.ShowStatisticsOption;
import com.degoos.chess.user.User;
import com.degoos.chess.util.ScannerUtils;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

public class RankingMenu extends Menu {

    private ShowStatisticsOption statisticsOption;
    private boolean sortByWinRate;

    public RankingMenu(Scanner scanner) {
        super(scanner);
        sortByWinRate = true;
        statisticsOption = new ShowStatisticsOption();
    }

    @Override
    public void run() {
        String option;
        do {
            showUsers();
            System.out.println("Insert an username to view a user's statistics.");
            System.out.print("Insert \"sort\" to sort by ");
            System.out.println(sortByWinRate ? "wins." : "win rate.");
            System.out.println("Insert \"exit\" to exit.");

            option = ScannerUtils.nextLine(scanner);
            if (option.equalsIgnoreCase("sort")) {
                sortByWinRate = !sortByWinRate;
            } else if (!option.equalsIgnoreCase("exit")) {
                Optional<User> user = Chess.getUserManager().getUser(option);
                if (user.isPresent()) {
                    statisticsOption.run(user.get());
                } else {
                    System.out.println("The user " + option + " doesn't exist!");
                }
            }
        } while (!option.equalsIgnoreCase("exit"));
    }

    private void showUsers() {
        List<User> users;

        if (sortByWinRate) {
            users = Chess.getUserManager().getUsers().values()
                    .stream().filter(target -> target.getTriedProblems() > 0)
                    .sorted((o1, o2) -> {
                        float w1 = o1.getWinRate();
                        float w2 = o2.getWinRate();
                        if(Float.isNaN(w1)) return 1;
                        if(Float.isNaN(w2)) return -1;
                        if (w1 == w2) return 0;
                        return w2 - w1 > 0 ? 1 : -1;
                    }).collect(Collectors.toList());
        } else {
            users = Chess.getUserManager().getUsers().values().stream().sorted((o1, o2) ->
                    o2.getSolvedProblems() - o1.getSolvedProblems()).collect(Collectors.toList());
        }

        float winRate;
        for (User user : users) {
            System.out.print("- " + user.getName() + "\t\t\t\tWins: " + user.getSolvedProblems());
            winRate = user.getWinRate();
            if (winRate == winRate) { //Is not NaN
                System.out.println("\t\t\t\tWR: " + winRate + "%");
            } else {
                System.out.println();
            }
        }
    }
}
