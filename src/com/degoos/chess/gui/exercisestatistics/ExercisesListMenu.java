package com.degoos.chess.gui.exercisestatistics;

import com.degoos.chess.Chess;
import com.degoos.chess.exercise.Exercise;
import com.degoos.chess.gui.Menu;
import com.degoos.chess.util.ScannerUtils;

import java.util.Comparator;
import java.util.Optional;
import java.util.Scanner;

public class ExercisesListMenu extends Menu {


    public ExercisesListMenu(Scanner scanner) {
        super(scanner);
    }

    @Override
    public void run() {
        String option;
        do {
            System.out.println("Exercises: ");
            showExercises();
            System.out.println("Insert an exercise name to view an exercise's statistics.");
            System.out.println("Insert \"exit\" to exit.");

            option = ScannerUtils.nextLine(scanner);

            if (option.equalsIgnoreCase("exit")) continue;

            Optional<Exercise> exercise = Chess.getExerciseManager().getExercise(option);
            if (exercise.isPresent()) {
                new ExerciseStatisticsMenu(scanner, exercise.get()).run();
            } else {
                System.out.println("This exercise doesn't exist!");
            }
        } while (!option.equalsIgnoreCase("exit"));
    }

    private void showExercises() {
        Chess.getExerciseManager().getExercises().values().stream().sorted(Comparator.comparing(Exercise::getName)).forEach(target -> System.out.println(" - " + target.getName()));
    }
}
