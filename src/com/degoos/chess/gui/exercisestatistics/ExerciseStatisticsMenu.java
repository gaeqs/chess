package com.degoos.chess.gui.exercisestatistics;

import com.degoos.chess.Chess;
import com.degoos.chess.exercise.Exercise;
import com.degoos.chess.gui.Menu;
import com.degoos.chess.gui.main.ShowStatisticsOption;
import com.degoos.chess.user.User;
import com.degoos.chess.util.ScannerUtils;

import java.util.Comparator;
import java.util.Optional;
import java.util.Scanner;

public class ExerciseStatisticsMenu extends Menu {

    private Exercise exercise;
    private ShowStatisticsOption statisticsOption;

    public ExerciseStatisticsMenu(Scanner scanner, Exercise exercise) {
        super(scanner);
        this.exercise = exercise;
        statisticsOption = new ShowStatisticsOption();
    }

    @Override
    public void run() {
        String option;
        do {
            System.out.println("Exercise's statistics:");
            System.out.println("- Tried: " + exercise.getTriedTimes() + " times.");
            System.out.println("- Solved: " + exercise.getSolvedTimes() + " times.");

            float solvedRate = exercise.getSolveRate();

            System.out.println("- Solve rate: " + (solvedRate != solvedRate ? "XX" : solvedRate) + "%"); //If is NaN, then XX%
            System.out.println("Winners:");
            showWinners();
            System.out.println("Insert an username to view a user's statistics.");
            System.out.println("Insert \"play\" to play this exercise.");
            System.out.println("Insert \"exit\" to go back to the exercises menu.");

            option = ScannerUtils.nextLine(scanner);

            if(option.equalsIgnoreCase("play")) {
                exercise.run(scanner, Chess.getCurrentUser());
                continue;
            }
            if (option.equalsIgnoreCase("exit")) continue;

            Optional<User> user = Chess.getUserManager().getUser(option);
            if (user.isPresent() && exercise.getWinners().contains(user.get())) {
                statisticsOption.run(user.get());
            } else {
                System.out.println("The user " + option + " doesn't exist in the winner list!");
            }

        } while (!option.equalsIgnoreCase("exit") && !option.equalsIgnoreCase("play"));
    }

    private void showWinners() {
        exercise.getWinners().stream().sorted(Comparator.comparing(User::getName)).forEach(target -> System.out.println(" - " + target.getName()));
    }
}
