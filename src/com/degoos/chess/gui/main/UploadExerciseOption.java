package com.degoos.chess.gui.main;

import com.degoos.chess.Chess;
import com.degoos.chess.gui.Option;
import com.degoos.chess.loader.ExerciseLoadResult;
import com.degoos.chess.loader.ExerciseLoadResultType;
import com.degoos.chess.util.ScannerUtils;

import java.util.Scanner;

public class UploadExerciseOption extends Option {

	public UploadExerciseOption() {
		super("Upload exercise.");
	}

	@Override
	public void run(Scanner scanner) {
		System.out.print("Insert the file's path: ");
		String url = ScannerUtils.nextLine(scanner);

		ExerciseLoadResult result = Chess.getExerciseLoader().load(url, null);

		if (result.getType() == ExerciseLoadResultType.SUCCESS) {
			if (Chess.getExerciseManager().addExercise(result.getExercise())) {
				System.out.println("Exercise added to the app!");
			} else {
				System.out.println("An exercise with the name " + result.getExercise().getName() + " was already found in the data!");
			}
		} else {
			System.out.println("Error while uploading the exercise!");
			System.out.println(result.getType() + ": " + result.getDetails());
		}
	}
}
