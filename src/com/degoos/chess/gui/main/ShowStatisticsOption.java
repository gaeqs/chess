package com.degoos.chess.gui.main;

import com.degoos.chess.Chess;
import com.degoos.chess.gui.Option;
import com.degoos.chess.user.User;

import java.util.Scanner;

public class ShowStatisticsOption extends Option {

    public ShowStatisticsOption() {
        super("Show statistics.");
    }

    @Override
    public void run(Scanner scanner) {
        run(Chess.getCurrentUser());
    }

    public void run(User user) {
        System.out.println(user.getName() + "'s statistics:");
        System.out.println(" - Tried problems: " + user.getTriedProblems());
        System.out.println(" - Solved problems: " + user.getSolvedProblems());
        System.out.println(" - Failed problems: " + (user.getTriedProblems() - user.getSolvedProblems()));
        System.out.println(" - Win rate: " + (user.getTriedProblems() == 0 ? "XX" : user.getWinRate() + "%"));
    }
}
