package com.degoos.chess.gui.main;

import com.degoos.chess.Chess;
import com.degoos.chess.exercise.Exercise;
import com.degoos.chess.gui.Option;

import java.util.Optional;
import java.util.Scanner;

public class RandomExerciseOption extends Option {

    public RandomExerciseOption() {
        super("Solve random problem.");
    }

    @Override
    public void run(Scanner scanner) {
        Optional<Exercise> exercise = Chess.getExerciseManager().getRandomExercise();
        if(exercise.isPresent()) {
            exercise.get().run(scanner, Chess.getCurrentUser());
        }
        else {
            System.out.println("There's no exercises to do!");
        }
    }
}
