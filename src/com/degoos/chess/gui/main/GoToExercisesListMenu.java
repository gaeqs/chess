package com.degoos.chess.gui.main;

import com.degoos.chess.gui.Option;
import com.degoos.chess.gui.exercisestatistics.ExercisesListMenu;

import java.util.Scanner;

public class GoToExercisesListMenu extends Option {

    public GoToExercisesListMenu() {
        super("Exercises statistics.");
    }

    @Override
    public void run(Scanner scanner) {
        new ExercisesListMenu(scanner).run();
    }
}
