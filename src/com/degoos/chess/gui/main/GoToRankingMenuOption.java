package com.degoos.chess.gui.main;

import com.degoos.chess.gui.Option;
import com.degoos.chess.gui.ranking.RankingMenu;

import java.util.Scanner;

public class GoToRankingMenuOption extends Option {

    public GoToRankingMenuOption() {
        super("Ranking.");
    }

    @Override
    public void run(Scanner scanner) {
        new RankingMenu(scanner).run();
    }
}
