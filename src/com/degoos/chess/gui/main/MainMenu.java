package com.degoos.chess.gui.main;

import com.degoos.chess.Chess;
import com.degoos.chess.gui.Option;
import com.degoos.chess.gui.OptionMenu;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class MainMenu extends OptionMenu {

    public MainMenu(Scanner scanner) {
        super(scanner, createOptions());
    }

    @Override
    public void run() {
        super.run();
        Chess.setCurrentUser(null);
    }

    private static List<Option> createOptions() {
        List<Option> list = new LinkedList<>();
        list.add(new RandomExerciseOption());
        list.add(new ShowStatisticsOption());
        list.add(new GoToExercisesListMenu());
        list.add(new GoToRankingMenuOption());
        list.add(new UploadExerciseOption());
        return list;
    }
}
