package com.degoos.chess.game.piece;

import com.degoos.chess.game.Position;
import com.degoos.chess.game.Table;
import com.degoos.chess.game.Team;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Knight extends Piece {

    /**
     * Creates a knight using a table, a team and a position.
     *
     * @param table    the table.
     * @param team     the team.
     * @param position the position.
     */
    public Knight(Table table, Team team, Position position) {
        super(table, team, position);
    }

    @Override
    public char getSimpleCharacter() {
        return 'N';
    }

    @Override
    public char getCharacter() {
        return team == Team.WHITE ? '♘' : '♞';
    }

    @Override
    public List<Position> getPossibleMovements() {
        List<Position> list = new LinkedList<>();

        addIfValid(list, -1, 2);
        addIfValid(list, 1, 2);

        addIfValid(list, 2, 1);
        addIfValid(list, 2, -1);

        addIfValid(list, 1, -2);
        addIfValid(list, -1, -2);

        addIfValid(list, -2, -1);
        addIfValid(list, -2, 1);

        return list;
    }

    private void addIfValid(List<Position> list, int x, int y) {
        Optional<Position> current = Position.getPositionIfValid(position, x, y);
        if (!current.isPresent()) return;
        Optional<Piece> piece = table.getPiece(current.get());
        if(!piece.isPresent() || piece.get().team != team) {
            list.add(current.get());
        }
    }

    @Override
    public Knight copy(Table table) {
        return new Knight(table, team, position);
    }
}
