package com.degoos.chess.game.piece;

import com.degoos.chess.game.Position;
import com.degoos.chess.game.Table;
import com.degoos.chess.game.Team;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class King extends Piece {

    /**
     * Creates a king using a table, a team and a position.
     *
     * @param table    the table.
     * @param team     the team.
     * @param position the position.
     */
    public King(Table table, Team team, Position position) {
        super(table, team, position);
    }

    @Override
    public char getSimpleCharacter() {
        return 'K';
    }

    @Override
    public char getCharacter() {
        return team == Team.WHITE ? '♔' : '♚';
    }

    @Override
    public List<Position> getPossibleMovements() {
        List<Position> list = new LinkedList<>();

        Optional<Position> current;
        Optional<Piece> piece;

        for (int x = -1; x < 2; x++) {
            for (int y = -1; y < 2; y++) {
                if (x == 0 && y == 0) continue;
                current = Position.getPositionIfValid(position, x, y);
                if (!current.isPresent()) continue;
                piece = table.getPiece(current.get());
                if (!piece.isPresent() || piece.get().team != team) {
                    list.add(current.get());
                }
            }
        }

        return list;
    }

    /**
     * Returns whether the king is in check.
     * @return  whether the king is in check.
     */
    public boolean inCheck() {
        return inCheck(team == Team.WHITE ? table.getBlackPieces() : table.getWhitePieces());
    }

    private boolean inCheck(List<Piece> enemies) {
        return enemies.stream().anyMatch(target -> target.getPossibleMovements().contains(position));
    }

    /**
     * Returns whether the king is in checkmate.
     * @return  whether the king is in checkmate.
     */
    public boolean inCheckmate() {
        List<Piece> enemies = team == Team.WHITE ? table.getBlackPieces() : table.getWhitePieces();
        //1
        if (!inCheck(enemies)) return false;

        //System.out.println(1);

        //2
        for (Position pos : getPossibleMovements()) {
           table.move(this, pos);
           if(!inCheck()) {
               table.revertLastMovement();
               return false;
           }
           table.revertLastMovement();
        }

        //System.out.println(2);

        //3
        List<Piece> allies = team == Team.WHITE ? table.getWhitePieces() : table.getBlackPieces();

        for (Piece ally : allies) {
            for (Position possibleMovement : ally.getPossibleMovements()) {
                table.move(ally, possibleMovement);
                if (!inCheck()) {
                    table.revertLastMovement();
                    System.out.println(ally.getCharacter() +": " +ally.getPosition() + " -> "+possibleMovement);
                    return false;
                }
                table.revertLastMovement();
            }
        }

        //System.out.println(3);

        return true;
    }

    @Override
    public King copy(Table table) {
        return new King(table, team, position);
    }
}
