package com.degoos.chess.game.piece;

import com.degoos.chess.game.Position;
import com.degoos.chess.game.Table;
import com.degoos.chess.game.Team;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Pawn extends Piece {

    /**
     * Creates a pawn using a table, a team and a position.
     *
     * @param table    the table.
     * @param team     the team.
     * @param position the position.
     */
    public Pawn(Table table, Team team, Position position) {
        super(table, team, position);
    }

    @Override
    public char getSimpleCharacter() {
        return 'P';
    }

    @Override
    public char getCharacter() {
        return team == Team.WHITE ? '♙' : '♟';
    }

    @Override
    public List<Position> getPossibleMovements() {
        List<Position> list = new LinkedList<>();

        Optional<Position> optional = Position.getPositionIfValid(position, 0, team == Team.WHITE ? 1 : -1);

        if(optional.isPresent() && !table.getPiece(optional.get()).isPresent()) {
            list.add(optional.get());
        }

        if(team == Team.WHITE && position.getY() == 1 || team == Team.BLACK && position.getY() == 6) {
            optional = Position.getPositionIfValid(position, 0, team == Team.WHITE ? 2 : -2);
            if(optional.isPresent() && !table.getPiece(optional.get()).isPresent() && !list.isEmpty()) {
                list.add(optional.get());
            }
        }

        //Side kills
        int y = team == Team.WHITE ? 1 : -1;
        optional = Position.getPositionIfValid(position, 1, y);
        if(optional.isPresent() && table.getPiece(optional.get()).filter(target -> target.team != team).isPresent()) {
            list.add(optional.get());
        }

        optional = Position.getPositionIfValid(position, -1, y);
        if(optional.isPresent() && table.getPiece(optional.get()).filter(target -> target.team != team).isPresent()) {
            list.add(optional.get());
        }

        return list;
    }

    @Override
    public Pawn copy(Table table) {
        return new Pawn(table, team, position);
    }
}
