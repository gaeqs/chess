package com.degoos.chess.game.piece;

import com.degoos.chess.game.Position;
import com.degoos.chess.game.Table;
import com.degoos.chess.game.Team;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Bishop extends Piece {

	/**
	 * Creates a bishop using a table, a team and a position.
	 *
	 * @param table    the table.
	 * @param team     the team.
	 * @param position the position.
	 */
	public Bishop(Table table, Team team, Position position) {
		super(table, team, position);
	}

	@Override
	public char getSimpleCharacter() {
		return 'B';
	}

	@Override
	public char getCharacter() {
		return team == Team.WHITE ? '♗' : '♝';
	}

	@Override
	public List<Position> getPossibleMovements() {
		List<Position> list = new LinkedList<>();

		Optional<Position> current;
		boolean found;
		Optional<Piece> piece;

		for (int i = 0; i < 4; i++) {
			found = false;
			int x, y;

			switch (i) {
				case 0:
					x = 1;
					y = 1;
					break;
				case 1:
					x = 1;
					y = -1;
					break;
				case 2:
					x = -1;
					y = -1;
					break;
				default:
					x = -1;
					y = 1;
					break;
			}

			current = Position.getPositionIfValid(position, x, y);

			while (current.isPresent() && !found) {
				piece = table.getPiece(current.get());
				if (piece.isPresent()) {
					found = true;

					if (piece.get().team != team) {
						list.add(current.get());
					}

					continue;
				}
				list.add(current.get());
				current = Position.getPositionIfValid(current.get(), x, y);
			}
		}

		return list;
	}

	@Override
	public Bishop copy(Table table) {
		return new Bishop(table, team, position);
	}
}
