package com.degoos.chess.game.piece;

import com.degoos.chess.game.Position;
import com.degoos.chess.game.Table;
import com.degoos.chess.game.Team;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Queen extends Piece {

    /**
     * Creates a queen using a table, a team and a position.
     *
     * @param table    the table.
     * @param team     the team.
     * @param position the position.
     */
    public Queen(Table table, Team team, Position position) {
        super(table, team, position);
    }

    @Override
    public char getSimpleCharacter() {
        return 'Q';
    }

    @Override
    public char getCharacter() {
        return team == Team.WHITE ? '♕' : '♛';
    }

    @Override
    public List<Position> getPossibleMovements() {
        List<Position> list = new LinkedList<>();

        Optional<Position> current;
        boolean found;
        Optional<Piece> piece;

        for (int x = -1; x < 2; x++) {
            for (int y = -1; y< 2; y++) {
                if(x == 0 && y == 0) continue;
                found = false;
                current = Position.getPositionIfValid(position, x, y);

                while (current.isPresent() && !found) {
                    piece = table.getPiece(current.get());
                    if (piece.isPresent()) {
                        found = true;

                        if (piece.get().team != team) {
                            list.add(current.get());
                        }

                        continue;
                    }
                    list.add(current.get());
                    current = Position.getPositionIfValid(current.get(), x, y);
                }
            }
        }

        return list;
    }

    @Override
    public Queen copy(Table table) {
        return new Queen(table, team, position);
    }
}
