package com.degoos.chess.game.piece;

import com.degoos.chess.game.Position;
import com.degoos.chess.game.Table;
import com.degoos.chess.game.Team;

import java.util.List;

public abstract class Piece {

    /**
     * Creates a piece using its character representation.
     *
     * @param table          the table of the piece.
     * @param team           the team of the piece.
     * @param representation the character representation.
     * @param position       the position of the piece.
     * @return the piece or null if the representation is invalid.
     */
    public static Piece createPiece(Table table, Team team, char representation, Position position) {
        switch (Character.toLowerCase(representation)) {
            case 'b':
                return new Bishop(table, team, position);
            case 'k':
                return new King(table, team, position);
            case 'n':
                return new Knight(table, team, position);
            case 'p':
                return new Pawn(table, team, position);
            case 'q':
                return new Queen(table, team, position);
            case 'r':
                return new Rook(table, team, position);
            default:
                return null;
        }
    }

    /**
     * Returns whether the given char representation is valid.
     *
     * @param representation the representation.
     * @return whether the given char representation is valid.
     */
    public static boolean isLegalCharacter(char representation) {
        representation = Character.toLowerCase(representation);
        return representation == 'b' || representation == 'k' || representation == 'n' ||
                representation == 'p' || representation == 'q' || representation == 'r';
    }


    protected Table table;
    protected Team team;
    protected Position position;

    /**
     * Creates a piece using a table, a team and a position.
     *
     * @param table    the table.
     * @param team     the team.
     * @param position the position.
     */
    public Piece(Table table, Team team, Position position) {
        this.table = table;
        this.team = team;
        this.position = position;
    }

    /**
     * Returns the table of the piece.
     *
     * @return the table of the piece.
     */
    public Table getTable() {
        return table;
    }

    /**
     * Returns the team of the piece.
     *
     * @return th team of the piece.
     */
    public Team getTeam() {
        return team;
    }

    /**
     * Returns the position of the piece.
     *
     * @return the position of te piece.
     */
    public Position getPosition() {
        return position;
    }

    /**
     * Sets the position of the piece.
     *
     * @param position the position of the piece.
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * Returns the save representation of the piece.
     *
     * @return the save representation.
     */
    public String getSaveString() {
        return (team == Team.BLACK ? "B" : "W") + getSimpleCharacter() + position.toString();
    }

    /**
     * Returns the simple representation of the piece. This will
     * be an ASCII character.
     *
     * @return the simple representation.
     */
    public abstract char getSimpleCharacter();

    /**
     * Returns the display representation of the piece. This may be an unicode character.
     *
     * @return the display representation.
     */
    public abstract char getCharacter();

    /**
     * Returns a list with all possibles movements the piece can perform.
     *
     * @return a list with all possibles movements the piece can perform.
     */
    public abstract List<Position> getPossibleMovements();

    /**
     * Creates a new instance of the piece for the given table.
     *
     * @param table the table.
     * @return the new instance.
     */
    public abstract Piece copy(Table table);
}
