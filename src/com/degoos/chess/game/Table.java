package com.degoos.chess.game;

import com.degoos.chess.game.piece.Pawn;
import com.degoos.chess.game.piece.Piece;

import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Stack;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Represents a chess table.
 */
public class Table {

    /**
     * The chess table length.
     */
    public static final int TABLE_LENGTH = 8;

    private Piece[][] positions;
    private List<Piece> whitePieces, blackPieces;
    private Stack<Movement> movements;

    /**
     * Creates a chess table.
     */
    public Table() {
        positions = new Piece[TABLE_LENGTH][TABLE_LENGTH];
        whitePieces = new LinkedList<>();
        blackPieces = new LinkedList<>();
        movements = new Stack<>();
    }

    /**
     * Creates a chess table copying all the data from another {@link Table} instance.
     * This performs a deep copy, copying all pieces.
     *
     * @param other the table to copy.
     */
    public Table(Table other) {
        positions = new Piece[TABLE_LENGTH][TABLE_LENGTH];
        whitePieces = new LinkedList<>();
        blackPieces = new LinkedList<>();

        other.whitePieces.forEach(target -> addPiece(target.copy(this)));
        other.blackPieces.forEach(target -> addPiece(target.copy(this)));
    }

    /**
     * Returns the white pieces of the table.
     *
     * @return the white pieces.
     */
    public List<Piece> getWhitePieces() {
        return whitePieces;
    }

    /**
     * Returns the black pieces of the table.
     *
     * @return the black pieces.
     */
    public List<Piece> getBlackPieces() {
        return blackPieces;
    }

    /**
     * Returns all white pieces that pass the given predicate.
     *
     * @param predicate the predicate.
     * @return the list of white pieces.
     */
    public List<Piece> getWhitePiecesThat(Predicate<? super Piece> predicate) {
        return whitePieces.stream().filter(predicate).collect(Collectors.toList());
    }

    /**
     * Returns all black pieces that pass the given predicate.
     *
     * @param predicate the predicate.
     * @return the black of white pieces.
     */
    public List<Piece> getBlackPiecesThat(Predicate<? super Piece> predicate) {
        return blackPieces.stream().filter(predicate).collect(Collectors.toList());
    }

    /**
     * Adds a piece to the table.
     *
     * @param piece the piece.
     */
    public void addPiece(Piece piece) {
        if (getPiece(piece.getPosition()).isPresent()) return;
        Position position = piece.getPosition();
        positions[position.getX()][position.getY()] = piece;

        if (piece.getTeam() == Team.WHITE) {
            whitePieces.add(piece);
        } else {
            blackPieces.add(piece);
        }
    }

    /**
     * Returns the piece in the given position if present.
     *
     * @param position the position.
     * @return the piece if present.
     */
    public Optional<Piece> getPiece(Position position) {
        return Optional.ofNullable(positions[position.getX()][position.getY()]);
    }

    /**
     * Moves a piece to the given position.
     *
     * @param piece    the piece to move.
     * @param position the position.
     * @return the piece that was in the position.
     */
    public Optional<Piece> move(Piece piece, Position position) {
        return move(piece, position, '-');
    }

    /**
     * Moves a piece to the given position.
     *
     * @param piece                     the piece to move.
     * @param position                  the position.
     * @param transformationIfPawnAtEnd the piece representation of the pawn transformation
     *                                  if the piece to move is a pawn and it is at the end of the table.
     * @return the piece that was in the position.
     */
    public Optional<Piece> move(Piece piece, Position position, char transformationIfPawnAtEnd) {
        Piece beforeTransformation = null;
        Position old = piece.getPosition();
        positions[old.getX()][old.getY()] = null;

        //Transform pawn
        if (piece instanceof Pawn && position.getY() == (piece.getTeam() == Team.WHITE ? 7 : 0)) {
            beforeTransformation = piece;
            piece = Piece.createPiece(this, piece.getTeam(), transformationIfPawnAtEnd, position);
            if (piece == null)
                throw new IllegalStateException("Error while creating piece " + transformationIfPawnAtEnd + "!");

            if (piece.getTeam() == Team.WHITE) {
                whitePieces.remove(beforeTransformation);
                whitePieces.add(piece);
            } else {
                blackPieces.remove(beforeTransformation);
                blackPieces.add(piece);
            }
        }

        Piece oldPiece = positions[position.getX()][position.getY()];
        positions[position.getX()][position.getY()] = piece;
        piece.setPosition(position);

        Movement mov;

        //Edit piece lists and adds the movement to the stack.
        if (oldPiece != null) {
            if (oldPiece.getTeam() == Team.WHITE) {
                whitePieces.remove(oldPiece);
            } else {
                blackPieces.remove(oldPiece);
            }

            if (beforeTransformation != null) {
                mov = new Movement(new Piece[]{beforeTransformation, piece, oldPiece}, new Position[]{old, null, position}, new Position[]{null, position, null});
            } else {
                mov = new Movement(new Piece[]{piece, oldPiece}, new Position[]{old, position}, new Position[]{position, null});
            }
        } else {
            if (beforeTransformation != null) {
                mov = new Movement(new Piece[]{beforeTransformation, piece}, new Position[]{old, null}, new Position[]{null, position});
            } else {
                mov = new Movement(new Piece[]{piece}, new Position[]{old}, new Position[]{position});
            }
        }

        movements.push(mov);

        return Optional.ofNullable(oldPiece);
    }

    /**
     * Reverts the last executed movement if present.
     */
    public void revertLastMovement() {
        if (movements.isEmpty()) return;
        movements.pop().revert(this, positions);
    }

    /**
     * Saves all pieces of the table on the give writer.
     * @param writer the writer.
     * @throws IOException if a write exception occurs.
     */
    public void save(Writer writer) throws IOException {
        for (Piece blackPiece : blackPieces) {
            writer.write(blackPiece.getSaveString() + '\n');
        }


        boolean first = true;
        for (Piece whitePiece : whitePieces) {
            if (first) {
                first = false;
            } else {
                writer.write('\n');
            }
            writer.write(whitePiece.getSaveString());
        }
    }

    /**
     * Prints the table.
     * @param error whether the table should be printed on the default or error stream.
     */
    public void print(boolean error) {
        PrintStream out = error ? System.err : System.out;

        for (int i = 0; i < TABLE_LENGTH * 4; i++) {
            out.print('_');
        }
        out.println();

        Piece piece;
        for (int row = TABLE_LENGTH - 1; row >= 0; row--) {
            out.print('|');
            for (int column = 0; column < TABLE_LENGTH; column++) {
                piece = positions[column][row];
                if (piece != null) {
                    out.print(piece.getCharacter());
                }
                out.print("\t|");
            }
            out.println();

            for (int i = 0; i < TABLE_LENGTH * 4; i++) {
                out.print('_');
            }
            out.println();
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < TABLE_LENGTH * 5 - 2; i++) {
            builder.append('_');
        }
        builder.append('\n');

        Piece piece;
        for (int row = TABLE_LENGTH - 1; row >= 0; row--) {
            builder.append('|');
            for (int column = 0; column < TABLE_LENGTH; column++) {
                piece = positions[column][row];
                if (piece != null) {
                    builder.append("  ").append(piece.getCharacter());
                }
                builder.append("\t|");
            }
            builder.append('\n');

            for (int i = 0; i < TABLE_LENGTH * 5 - 2; i++) {
                builder.append('_');
            }
            builder.append('\n');
        }
        return builder.toString();
    }
}
