package com.degoos.chess.game;

/**
 * Represents a chess team.
 */
public enum Team {

    WHITE, BLACK;

    /**
     * Returns a {@link Team} by its character representation.
     *
     * @param c the character.
     * @return the team or null.
     */
    public static Team parse(char c) {
        switch (c) {
            case 'w':
            case 'W':
                return Team.WHITE;
            case 'b':
            case 'B':
                return Team.BLACK;
            default:
                return null;
        }
    }
}
