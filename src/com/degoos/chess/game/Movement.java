package com.degoos.chess.game;

import com.degoos.chess.game.piece.Piece;

/**
 * Represents a chess movement.
 */
public class Movement {

    private Piece[] pieces;
    private Position[] before;
    private Position[] after;

    /**
     * Creates a movement with all involved pieces, their old positions and their new position.
     *
     * @param pieces the pieces.
     * @param before the old position (or null if not present).
     * @param after  the new position (or null if not present).
     */
    public Movement(Piece[] pieces, Position[] before, Position[] after) {
        this.pieces = pieces;
        this.before = before;
        this.after = after;
    }

    /**
     * Returns the involved pieces.
     *
     * @return the involved pieces.
     */
    public Piece[] getPieces() {
        return pieces;
    }

    /**
     * Returns the old positions of the involved pieces.
     *
     * @return the old positions.
     */
    public Position[] getBefore() {
        return before;
    }

    /**
     * Returns the new positions of the involved pieces.
     *
     * @return the new positions.
     */
    public Position[] getAfter() {
        return after;
    }


    /**
     * Revers the movement.
     * @param table the table of the movement.
     * @param tableData the table data. This is given to revert the movement faster.
     */
    public void revert(Table table, Piece[][] tableData) {
        for (Position position : after) {
            if (position == null) continue;
            tableData[position.getX()][position.getY()] = null;
        }
        Position position;
        Piece piece;
        for (int i = 0; i < pieces.length; i++) {
            position = before[i];
            piece = pieces[i];

            if (position == null) {
                if (piece.getTeam() == Team.WHITE) {
                    table.getWhitePieces().remove(piece);
                } else {
                    table.getBlackPieces().remove(piece);
                }
                continue;
            }

            tableData[position.getX()][position.getY()] = piece;
            piece.setPosition(position);

            if (after[i] == null) {
                if (piece.getTeam() == Team.WHITE) {
                    table.getWhitePieces().add(piece);
                } else {
                    table.getBlackPieces().add(piece);
                }
            }
        }
    }
}
