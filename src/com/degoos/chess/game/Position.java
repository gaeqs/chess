package com.degoos.chess.game;

import java.util.Objects;
import java.util.Optional;

/**
 * Represents a position on the chess table.
 */
public class Position {

    /**
     * Parse array for the x coordinate.
     */
    private static char[] X_POSITIONS = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};

    public static Optional<Position> getPositionIfValid(Position position, int x, int y) {
        return getPositionIfValid(position.x + x, position.y + y);
    }

    public static Optional<Position> getPositionIfValid(int x, int y) {
        try {
            return Optional.of(new Position(x, y));
        } catch (IllegalArgumentException ex) {
            return Optional.empty();
        }
    }

    private int x, y;

    /**
     * Creates a position using its integer representations.
     *
     * @param x the x param (0 - 7).
     * @param y the y param (0 - 7).
     */
    public Position(int x, int y) {
        if (x < 0 || x >= Table.TABLE_LENGTH) throw new IllegalArgumentException("X argument not valid: " + x);
        if (y < 0 || y >= Table.TABLE_LENGTH) throw new IllegalArgumentException("Y argument not valid: " + y);
        this.x = x;
        this.y = y;
    }

    /**
     * Creates a position using its character representations.
     *
     * @param xc the x param (a - h).
     * @param yc the y param (1 - 8).
     */
    public Position(char xc, char yc) {
        switch (Character.toLowerCase(xc)) {
            case 'a':
                x = 0;
                break;
            case 'b':
                x = 1;
                break;
            case 'c':
                x = 2;
                break;
            case 'd':
                x = 3;
                break;
            case 'e':
                x = 4;
                break;
            case 'f':
                x = 5;
                break;
            case 'g':
                x = 6;
                break;
            case 'h':
                x = 7;
                break;
            default:
                throw new IllegalArgumentException("Invalid x");
        }

        try {
            y = Integer.parseInt(String.valueOf(yc)) - 1;
        } catch (Exception ex) {
            throw new IllegalArgumentException("Invalid y");
        }

        if (y < 0 || y >= Table.TABLE_LENGTH)
            throw new IllegalArgumentException("Invalid y");
    }

    /**
     * Returns the x param.
     *
     * @return the x param.
     */
    public int getX() {
        return x;
    }

    /**
     * Returns a new {@link Position} with the x param modified.
     *
     * @param x the x param
     * @return the new {@link Position}.
     */
    public Position setX(int x) {
        return new Position(x, y);
    }

    /**
     * Returns the y param.
     *
     * @return the y param.
     */
    public int getY() {
        return y;
    }

    /**
     * Returns a new {@link Position} with the y param modified.
     *
     * @param y the y param
     * @return the new {@link Position}.
     */
    public Position setY(int y) {
        return new Position(x, y);
    }

    /**
     * Returns a new {@link Position} with the given translation applied.
     *
     * @param x the x translation.
     * @param y the y translation.
     * @return the new {@link Position}.
     */
    public Position add(int x, int y) {
        return new Position(x + this.x, y + this.y);
    }

    /**
     * Returns a new {@link Position} with the given translation applied.
     *
     * @param x the x translation.
     * @param y the y translation.
     * @return the new {@link Position}.
     */
    public Position subtract(int x, int y) {
        return add(-x, -y);
    }

    @Override
    public String toString() {
        return X_POSITIONS[x] + String.valueOf(y + 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x && y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
