package com.degoos.chess.util;

import java.io.File;

public class FileUtils {


	/**
	 * Checks if a file is a folder. If the file is a file or it doesn't exist, then the method creates a folder.
	 * @param folder the file to check.
	 */
	public static void checkFolder(File folder) {
		if (folder.isFile()) {
			folder.delete();
		}
		if (!folder.exists()) {
			folder.mkdirs();
		}
	}
}
