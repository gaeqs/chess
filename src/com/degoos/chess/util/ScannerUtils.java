package com.degoos.chess.util;

import java.util.Scanner;

public class ScannerUtils {

    /**
     * Reads a line from a {@link Scanner}, avoiding all jump lines or empty strings.
     * @param scanner the scanner to read.
     * @return the next line.
     */
    public static String nextLine (Scanner scanner) {
        String line;
        while ((line = scanner.nextLine().trim()).isEmpty() || line.equals("\n") || line.equals("\r"));
        return line;
    }

}
